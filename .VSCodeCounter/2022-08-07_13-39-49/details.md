# Details

Date : 2022-08-07 13:39:49

Directory c:\\Users\\47347\\Desktop\\low_code\\website\\src

Total : 21 files,  839 codes, 33 comments, 83 blanks, all 955 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [website/src/App.vue](/website/src/App.vue) | Vue | 10 | 0 | 6 | 16 |
| [website/src/components/editorArea/Area copy.vue](/website/src/components/editorArea/Area%20copy.vue) | Vue | 35 | 0 | 2 | 37 |
| [website/src/components/editorArea/ContextMenu.vue](/website/src/components/editorArea/ContextMenu.vue) | Vue | 104 | 0 | 16 | 120 |
| [website/src/components/editorArea/Grid.vue](/website/src/components/editorArea/Grid.vue) | Vue | 25 | 0 | 1 | 26 |
| [website/src/components/editorArea/Shape.vue](/website/src/components/editorArea/Shape.vue) | Vue | 156 | 1 | 13 | 170 |
| [website/src/components/editorArea/index.vue](/website/src/components/editorArea/index.vue) | Vue | 116 | 3 | 6 | 125 |
| [website/src/components/layout/AttributeArea.vue](/website/src/components/layout/AttributeArea.vue) | Vue | 11 | 0 | 3 | 14 |
| [website/src/components/layout/MaterialArea.vue](/website/src/components/layout/MaterialArea.vue) | Vue | 46 | 0 | 4 | 50 |
| [website/src/components/layout/ToolBar.vue](/website/src/components/layout/ToolBar.vue) | Vue | 11 | 0 | 4 | 15 |
| [website/src/main.js](/website/src/main.js) | JavaScript | 12 | 0 | 2 | 14 |
| [website/src/router/index.js](/website/src/router/index.js) | JavaScript | 15 | 0 | 5 | 20 |
| [website/src/store/index.js](/website/src/store/index.js) | JavaScript | 13 | 0 | 2 | 15 |
| [website/src/store/modules/component.js](/website/src/store/modules/component.js) | JavaScript | 30 | 0 | 1 | 31 |
| [website/src/store/modules/contextMenu.js](/website/src/store/modules/contextMenu.js) | JavaScript | 27 | 2 | 1 | 30 |
| [website/src/store/modules/editor.js](/website/src/store/modules/editor.js) | JavaScript | 20 | 0 | 0 | 20 |
| [website/src/style/iconfont.css](/website/src/style/iconfont.css) | CSS | 26 | 0 | 7 | 33 |
| [website/src/utils/components-list.js](/website/src/utils/components-list.js) | JavaScript | 46 | 27 | 1 | 74 |
| [website/src/utils/directives.js](/website/src/utils/directives.js) | JavaScript | 30 | 0 | 1 | 31 |
| [website/src/utils/element-ui.js](/website/src/utils/element-ui.js) | JavaScript | 17 | 0 | 2 | 19 |
| [website/src/utils/style.js](/website/src/utils/style.js) | JavaScript | 36 | 0 | 2 | 38 |
| [website/src/views/HomeView.vue](/website/src/views/HomeView.vue) | Vue | 53 | 0 | 4 | 57 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)