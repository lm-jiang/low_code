# Summary

Date : 2022-08-07 13:39:49

Directory c:\\Users\\47347\\Desktop\\low_code\\website\\src

Total : 21 files,  839 codes, 33 comments, 83 blanks, all 955 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Vue | 10 | 567 | 4 | 59 | 630 |
| JavaScript | 10 | 246 | 29 | 17 | 292 |
| CSS | 1 | 26 | 0 | 7 | 33 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 21 | 839 | 33 | 83 | 955 |
| components | 8 | 504 | 4 | 49 | 557 |
| components\\editorArea | 5 | 436 | 4 | 38 | 478 |
| components\\layout | 3 | 68 | 0 | 11 | 79 |
| router | 1 | 15 | 0 | 5 | 20 |
| store | 4 | 90 | 2 | 4 | 96 |
| store\\modules | 3 | 77 | 2 | 2 | 81 |
| style | 1 | 26 | 0 | 7 | 33 |
| utils | 4 | 129 | 27 | 6 | 162 |
| views | 1 | 53 | 0 | 4 | 57 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)