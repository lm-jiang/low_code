import store from "@/store";
export const draggable = {
  inserted: function (el) {
    el.style.cursor = "move";
    el.onmousedown = function (e) {
      let disx = e.pageX - el.offsetLeft;
      let disy = e.pageY - el.offsetTop;
      document.onmousemove = function (e) {
        let x = e.pageX - disx;
        let y = e.pageY - disy;
        let maxX = store.state.editor.width;
        let maxY = store.state.editor.height;
        if (x < 0) {
          x = 0;
        } else if (x > maxX) {
          x = maxX;
        }
        if (y < 0) {
          y = 0;
        } else if (y > maxY) {
          y = maxY;
        }
        store.dispatch("setCurComponentPosition", { x, y });
      };
      document.onmouseup = function (e) {
        document.onmousemove = document.onmouseup = null;
      };
    };
  },
};
export const scaling = {
  inserted: function (el, binding) {
    el.onmousedown = function (e) {
      e.stopPropagation();
      e.preventDefault();
      console.log(e)
      let disx = e.pageX - el.offsetLeft;
      let disy = e.pageY - el.offsetTop;
      let curComponent=store.state.menu.curComponent
      let {width,height}=curComponent.style
      document.onmousemove = function (e) {
        let lwidth = e.pageX - disx;
        let lheight = e.pageY - disy;       
        console.log(lwidth,lheight)
        // let maxX = store.state.editor.width;
        // let maxY = store.state.editor.height;
        switch (binding.value) {
          case "lt":
            break;
          case "t":
            break;
          case "rt":
            break;
          case "r":
            break;
          case "rb":
            break;
          case "b":
            break;
          case "lb": //不执行任何操作
            break;
          case "l":
            break;
        }
        width=lwidth
        height=lheight
        store.dispatch("setCurComponentSize", { width, height });
      };
      document.onmouseup = function (e) {
        document.onmousemove = document.onmouseup = null;
      };
    };
  },
};
