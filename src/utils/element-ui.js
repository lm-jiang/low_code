import Vue from 'vue'
import {
    Container,
    Header,
    Aside,
    Main,
    Button,
    Input,
    Icon
} from 'element-ui'


Vue.use(Container)
Vue.use(Header)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Button)
Vue.use(Input)
Vue.use(Icon)