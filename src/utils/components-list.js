// 左侧物料区的组件列表
// {
//     component: "", //组件名称，需提前注册到vue
//     label: "", //左侧物料区显示的名字
//     propValue:'',//预置的插槽内容
//     type:'', //组件的类型   不同类型的组件，不同的渲染方式,   image/button/input...
//     icon: "", //左侧物料区中显示的图标
//     animations: [], //动画列表
//     events: {}, //事件列表
//     style: {
//       // 样式表，根据后续开发需求可对下列属性进行增删
//       width: "", //组件宽度
//       height: "", //组件高度
//       borderWidth:"",//边框大小
//       borderColor:"",//边框颜色
//       borderRadius:"",//圆角
//       fontSize: "", //文字大小
//       fontWeight: "", //字重   400-normal  700-bold
//       lineHeight: "", //行高
//       letterSpacing: 0, //字符间距
//       textAlign: "", //行内元素对齐方式
//       color: "", //字体颜色
//       background: "", //背景
//       top:'',//距离顶部画布的距离
//       left:''//距离左边画布的距离
//     },
//   },
const list = [
  {
    component: "el-button",
    label: "按钮",
    propValue: "Button",
    icon: "button",
    type:'button',
    animations: [],
    events: {},
    style: {
      width: 100,
      height: 34,
      borderWidth: 1,
      borderColor: "black",
      borderRadius: "",
      fontSize: 14,
      fontWeight: 400,
      lineHeight: "",
      letterSpacing: 0,
      textAlign: "",
      color: "",
      background: "",
      rotate:0
    },
  },
  {
    component: "el-input",
    label: "输入框",
    propValue: "请输入内容", //placeholder占位符
    icon: "input",
    type:'input',
    animations: [],
    events: {},
    style: {
      width: 180,
      height: 40,  //最小值，不能再小了
      fontSize: 14,
      fontWeight: 400,
      lineHeight: "",
      letterSpacing: 0,
      textAlign: "",
      color: "",
      background: "",
      rotate:0
    },
  },
];
export default list;
