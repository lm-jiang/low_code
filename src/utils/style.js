const needUnit = [
    'fontSize',
    'width',
    'height',
    'borderWidth',
    'letterSpacing',
    'borderRadius',
    'fontWeight',
]
export function getShapeStyle(style) {
    const result = {};
    ['width', 'height', 'top', 'left', 'rotate'].forEach(attr => {
        if (attr != 'rotate') {
            result[attr] = style[attr] + 'px'
        } else {
            result.transform = 'rotate(' + style[attr] + 'deg)'//+'  translate(-50%,-50%)'
        }
    })

    return result
}

export function getStyle(style, filter = []) {
    const result = {}
    Object.keys(style).forEach(key => {
        if (!filter.includes(key)) {    //违规属性过滤
            if (key != 'rotate') {
                result[key] = style[key]
                if (needUnit.includes(key)) {
                    result[key] += 'px' 
                }
            } else {
                result.transform = key + '(' + style[key] //旋转
            }
        }
    })
    return result
}