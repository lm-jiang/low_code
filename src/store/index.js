import Vue from 'vue'
import Vuex from 'vuex'
import component from './modules/component'
import menu from './modules/contextMenu'
import editor from './modules/editor'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    component,
    menu,
    editor
  }
})
