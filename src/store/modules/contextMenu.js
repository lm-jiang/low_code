const state = {
    menuTop:'',//菜单的位置
    menuLeft:'',
    menuShow:'',//菜单显示与隐藏
    curComponent:null//当前选中的组件
};
const mutations = {
    // 让菜单展示出来
    SHOW_MENU(state,value){
        state.menuShow=true,
        state.menuLeft=value.x
        state.menuTop=value.y
    },
    HIDDEN_MENU(state){
        state.menuShow=false
    },
    // 选中当前组件
    SELECT_CUR_COMPONENT(state,value=null){
        state.curComponent=value
    }
};
const actions = {};
const getters = {};
export default {
  state,
  mutations,
  actions,
  getters,
};
