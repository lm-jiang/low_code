const state = {
    width:1250, //画布的宽高
    height:800
};
const mutations = {};
const actions = {};
const getters = {
    editorWidth(state){
        return state.width+'px'
    },
    editorHeight(state){
        return state.height+'px'
    }
};
export default {
  state,
  mutations,
  actions,
  getters,
};