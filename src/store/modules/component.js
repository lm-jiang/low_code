const state = {
  componentData: [],
};
const mutations = {
  ADD_COMPONENT(state, value) {
    state.componentData.push(JSON.parse(JSON.stringify(value))); //巨坑！！！，不用深拷贝会覆盖之前的数据
  },
  SET_CUR_COMPONENT_POSITION(state, data) {
    state.componentData[data.index].style.left = data.x;
    state.componentData[data.index].style.top = data.y;
  },
  SET_CUR_COMPONENT_SIZE(state, data) {
    state.componentData[data.index].style.width = data.width;
    state.componentData[data.index].style.height = data.height;
  },
};
const actions = {
  setCurComponentPosition(context,data){
    let curComponentId=context.rootState.menu.curComponent.id
    let index=context.state.componentData.findIndex(item=>item.id===curComponentId)
    context.commit('SET_CUR_COMPONENT_POSITION',{index,...data})
  },
  setCurComponentSize(context,data){
    let curComponentId=context.rootState.menu.curComponent.id
    let index=context.state.componentData.findIndex(item=>item.id===curComponentId)
    context.commit('SET_CUR_COMPONENT_SIZE',{index,...data})
  }

};
const getters = {
  componentData(state) {
    return state.componentData || [];
  },
};
export default {
  state,
  mutations,
  actions,
  getters,
};
